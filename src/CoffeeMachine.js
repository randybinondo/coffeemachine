const partials = [
    '(Drink maker',
    'makes',
    '*countPerType*',
    '*type*',
    'with',
    '*countSugar*',
    'sugar',
    'and',
    '*countStick*',
    'stick)'
]

function CoffeeMachine(params) {
    if(!getIsInstructionValid(params)) return
    return getIsMessage(params) ?
    '"M:message-content" ' + `(Drink maker forwards any message received \nonto the coffee machine interface \nfor the customer to see)`
    :
    '"'+ params + '" ' + partials.join(' ')
        .replace('*countPerType*','1')
        .replace('*type*', getDrinkType(params))
        .replace('*countSugar*', getSugarCountText(params))
        .replace('and', getSugarCountText(params) === 'no' ? '- \nand' : 'and')
        .replace('sugar', getIsSugarPlural(params) ? 'sugars' : 'sugar')
        .replace('*countStick*', getStickText(params))
}

let getSugarCount = (params) => {
    const count = params.split(':').splice(1, 1)
    const value = Number(count)
    return isNaN(value) ? 0 : value
}

let getStickCount = (params) => {
    const count = getSugarCount(params)
    return count >= 1 ? 1 : 0
}

let getSugarCountText = (params) => {
    const count = getSugarCount(params)
    return count === 0 ? 'no' : count
}

let getDrinkType = (params) => {
    const instructionMap = {
        'T': 'tea',
        'H': 'chocolate',
        'C': 'coffee',
        'M': 'message'
    };

    const ins = params.split(':').splice(0, 1)

    return instructionMap[ins]
}

let getStickText = (params) => {
    const count = getStickCount(params)
    return count ? 'a' : 'therefore no'
}

let getIsSugarPlural = (params) => {
    return getSugarCount(params) > 1
}

let getIsMessage = (params) => {
    const type = getDrinkType(params)

    return type === 'message'
}

let getIsInstructionValid = (params) => {
    return typeof getDrinkType(params) !== 'undefined'
}

module.exports = {
    CoffeeMachine,
    getDrinkType,
    getSugarCount,
    getSugarCountText,
    getStickCount,
    getStickText,
    getIsSugarPlural,
    getIsMessage,
    getIsInstructionValid
}