const {
    CoffeeMachine,
    getDrinkType,
    getSugarCount,
    getSugarCountText,
    getStickCount,
    getStickText,
    getIsSugarPlural,
    getIsMessage,
    getIsInstructionValid
} = require('../src/CoffeeMachine');

test('test hot chocolate type instruction ', () => {
    expect(getDrinkType('H::')).toBe('chocolate')
})

test('test tea type instruction ', () => {
    expect(getDrinkType('T:1:0')).toBe('tea')
})

test('test coffee type instruction ', () => {
    expect(getDrinkType('C:2:0')).toBe('coffee')
})

test('test message type instruction ', () => {
    expect(getDrinkType('M:message-content')).toBe('message')
})

test('test sugar count from instruction ', () => {
    expect(getSugarCount('T:1:0')).toBe(1)
})

test('another test sugar count from instruction ', () => {
    expect(getSugarCount('C:2:0')).toBe(2)
})

test('another test sugar count with empty values from instruction ', () => {
    expect(getSugarCount('H::')).toBe(0)
})

test('test non-numeric value on sugar', () => {
    expect(getSugarCount('H:aa:')).toBe(0)
})

test('test plural sugar with empty values from instruction', () => {
    expect(getIsSugarPlural('H::')).toBe(false)
})

test('test plural sugar with value 1', () => {
    expect(getIsSugarPlural('T:1:0')).toBe(false)
})

test('test plural sugar with value greater than 1', () => {
    expect(getIsSugarPlural('C:2:0')).toBe(true)
})

test('another test plural sugar with value greater than 1', () => {
    expect(getIsSugarPlural('C:5:0')).toBe(true)
})

test('test sugar count text with empty values from instruction should return "no" ', () => {
    expect(getSugarCountText('H::')).toBe('no')
})

test('test stick count base on empty sugar count', () => {
    expect(getStickCount('H::')).toBe(0)
})

test('test stick count base on sugar count', () => {
    expect(getStickCount('T:1:0')).toBe(1)
})

test('test stick count base on more than 1 sugar count', () => {
    expect(getStickCount('C:2:0')).toBe(1)
})

test('test stick count text base on sugar count', () => {
    expect(getStickText('H::')).toBe('therefore no')
})

test('test stick count text base on 1 sugar count', () => {
    expect(getStickText('T:1:0')).toBe('a')
})

test('test stick count text base on more than 1 sugar count', () => {
    expect(getStickText('C:2:0')).toBe('a')
})

test('test isMessage instruction valid', () => {
    expect(getIsMessage('M:message-content')).toBe(true)
})

test('test isMessage instruction invalid', () => {
    expect(getIsMessage('T:message-content')).toBe(false)
})

test('test isValid instruction using only phrases', () => {
    expect(getIsInstructionValid('the quick brown fox')).toBe(false)
})

test('test isValid instruction using single colon with no type match', () => {
    expect(getIsInstructionValid('Q:the quick brown fox')).toBe(false)
})

test('test isValid instruction using single colon with type match', () => {
    expect(getIsInstructionValid('H:100:0')).toBe(true)
})

test('sends hot chocolate instructions', () => {
    expect(CoffeeMachine('H::')).toBe(`"H::" (Drink maker makes 1 chocolate with no sugar - \nand therefore no stick)`);
})

test('sends tea chocolate instructions', () => {
    expect(CoffeeMachine('T:1:0')).toBe(`"T:1:0" (Drink maker makes 1 tea with 1 sugar and a stick)`);
})

test('sends coffee instructions', () => {
    expect(CoffeeMachine('C:2:0')).toBe(`"C:2:0" (Drink maker makes 1 coffee with 2 sugars and a stick)`);
})

test('send message instruction', () => {
    expect(CoffeeMachine('M:message-content')).toBe(`"M:message-content" (Drink maker forwards any message received \nonto the coffee machine interface \nfor the customer to see)`)
})

test('send sample message instruction', () => {
    expect(CoffeeMachine('M:the quick brown fox')).toBe(`"M:message-content" (Drink maker forwards any message received \nonto the coffee machine interface \nfor the customer to see)`)
})

test('send invalid instruction', () => {
    expect(CoffeeMachine('the quick brown fox')).toBe(undefined)
})